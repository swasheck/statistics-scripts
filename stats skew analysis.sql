/*
	Stats Skew Analysis
*/


declare @SchemaName sysname = 'dbo' ,
    @TableName sysname  = 'f_word' ,
    @ColumnName sysname = 'word_id' ,
    @StatName sysname ,
    @StatColType sysname ,
    @StatColPrec int ,
    @StatColScale int ,
    @StatColMaxLen int ,
	@StatSteps tinyint,
    @TallyTable sysname ,	
    @sql nvarchar(max);

set @TallyTable = 'tempdb.dbo.[tt_'+@TableName+'_'+@ColumnName+']';

if exists (select 1 from tempdb.sys.tables where name = 'histo')
	begin
		drop table tempdb.dbo.histo
	end

if not exists (select 1 from tempdb.sys.tables where name = 'tt_'+@TableName+'_'+@ColumnName)
	begin
		set @sql = 
				'select 
						[key] = '+quotename(@ColumnName)+', 
						x = dense_rank() over (order by '+quotename(@ColumnName)+'), 
						f = count_big(1)			
				into  '+@TallyTable+'
				from '+quotename(@SchemaName)+'.'+quotename(@TableName)+'								
				group by '+quotename(@ColumnName)
		print @sql;

		exec sp_executesql @sql;

		-- indexing for fun and profit
		set @sql = 'create clustered index cix_temp on '+@TallyTable+'(x,f)';
		exec sp_executesql @sql;
	end

declare statcur cursor fast_forward read_only for
	select  SchemaName = sh.name ,
			TableName = t.name ,
			StatName = s.name ,                
			StatColumnType = isnull(bt.name, st.name) ,
			c.precision ,
			c.scale ,
			c.max_length ,
			sp.steps 			
	from    sys.stats s
			join sys.tables t 
				on s.object_id = t.object_id
			join sys.schemas sh 
				on sh.schema_id = t.schema_id
			join sys.columns c 
				on c.object_id = s.object_id
				and c.name = index_col(db_name(db_id()) + '.' + sh.name + '.' + t.name,s.stats_id, 1) 
			join sys.types st 
				on c.system_type_id = st.system_type_id
				and c.user_type_id = st.user_type_id
			cross apply sys.dm_db_stats_properties(s.object_id, s.stats_id) sp
			left join sys.types as bt 
				on st.is_user_defined = 1
				and bt.is_user_defined = 0
				and st.system_type_id = bt.system_type_id
				and st.user_type_id <> bt.user_type_id
	where   sh.name != 'sys'
			and t.name = @TableName
			and sh.name = @SchemaName
			and s.has_filter = 0
			and c.name = @ColumnName;        

open statcur
fetch next from statcur into 
	@SchemaName, @TableName,@StatName,@StatColType,@StatColPrec,@StatColScale,@StatColMaxLen,@StatSteps;


set @sql = 'create table tempdb.dbo.histo (
				table_name sysname null, 
				stat_name sysname null,
				range_hi_key '
				+
				case 
					when @StatColType in ('tinyint', 'smallint', 'int', 'bigint','datetime')
						then @StatColType
					when @StatColType in ('char', 'varchar', 'nchar', 'nvarchar')
						then @StatColType + '(' + 
							case 
								when @StatColMaxLen = 8000 then 'max) collate database_default'
								else cast(@StatColMaxLen as varchar(4)) + ') collate database_default'
							end

						--convert(varchar(4),isnull(nullif(@StatColMaxLen,8000),'''max''')) + ')'
					when @StatColType in ('datetime2', 'datetimeoffset', 'time')
						then @StatColType + '(' + convert(varchar(5),@StatColScale) + ')'
					when @StatColType in ('numeric', 'decimal')
						then @StatColType + '(' + convert(varchar(5),@StatColPrec) + ',' + convert(varchar(5), @StatColScale) + ')'
					else @StatColType
				end
				+
				', 
				range_rows decimal(30,3), 
				eq_rows bigint, 
				distinct_range_rows bigint,
				avg_range_rows decimal(30,3),
				actual_range_rows decimal(30,3),
				actual_eq_rows decimal(30,3),
				actual_distinct_range_rows bigint, 
				actual_avg_range_rows decimal(30,3),						
				b1_skewness decimal(30,3),
				G1_skewness decimal(30,3),
				ses decimal(30,5),
				Zg1 decimal(30,3),
				alt_key int null 										
			);';

print @sql;
execute sp_executesql @sql;

while @@fetch_status = 0 
	begin
		set @sql = 'dbcc show_statistics('''+quotename(@SchemaName)+'.'+quotename(@TableName)+''','''+@StatName+''') with histogram,no_infomsgs;'
		print @sql;		
		insert into tempdb.dbo.histo (
						range_hi_key, 
						range_rows , 
						eq_rows , 
						distinct_range_rows ,
						avg_range_rows )
		execute sp_executesql @sql

		set @sql = 
		'update h
			set h.actual_eq_rows = c.f,
				h.alt_key = c.x
		from tempdb.dbo.histo h
		join '+(@TallyTable)+' c 
			on h.range_hi_key = c.[key]';

		exec sp_executesql @sql;


		set @sql = '
		declare @thiskey '+
						case 
						when @StatColType in ('tinyint', 'smallint', 'int', 'bigint','datetime')
							then @StatColType
						when @StatColType in ('char', 'varchar', 'nchar', 'nvarchar')
							then @StatColType + '(' + convert(varchar(4),@StatColMaxLen) + ')'
						when @StatColType in ('datetime2', 'datetimeoffset', 'time')
							then @StatColType + '(' + convert(varchar(5),@StatColScale) + ')'
						when @StatColType in ('numeric', 'decimal')
							then @StatColType + '(' + convert(varchar(5),@StatColPrec) + ',' + convert(varchar(5), @StatColScale) + ')'
						else @StatColType
						end
						  +';
		declare @lastkey '+
						case 
						when @StatColType in ('tinyint', 'smallint', 'int', 'bigint','datetime')
							then @StatColType
						when @StatColType in ('char', 'varchar', 'nchar', 'nvarchar')
							then @StatColType + '(' + convert(varchar(4),@StatColMaxLen) + ')'
						when @StatColType in ('datetime2', 'datetimeoffset', 'time')
							then @StatColType + '(' + convert(varchar(5),@StatColScale) + ')'
						when @StatColType in ('numeric', 'decimal')
							then @StatColType + '(' + convert(varchar(5),@StatColPrec) + ',' + convert(varchar(5), @StatColScale) + ')'
						else @StatColType
						end
						  +';


		declare windowcur cursor fast_forward read_only
			for 
			select	range_hi_key 
			from tempdb.dbo.histo
			order by range_hi_key asc;
		open windowcur;
		fetch next from windowcur
			into @thiskey;
		while @@fetch_status = 0
			begin
					update tempdb.dbo.histo 
					set 
						actual_range_rows = isnull(n,0),
						actual_avg_range_rows = case when n is null then 0 else (1.*n)/d end,
						actual_distinct_range_rows = isnull(d,0),
						b1_skewness = sk.b1,
						G1_skewness = sk.G1,
						ses = sk.ses,
						Zg1 = case sk.ses when 0 then sk.G1 else sk.G1/sk.ses end
					from (
							select 
								b1 =
									case when (power(m2,1.5)) = 0 then 0 
									else m3 / (power(m2,1.5))
									end,
								G1 = 
									case n when 2 then 0 
									else (sqrt(1.*(n*(n-1)))/(n-2)) * 
										case when (power(m2,1.5)) = 0 then 0 
										else m3 / (power(m2,1.5))
										end
									end,
								ses = case when ((n-2.)*(n+1.)*(n+3.)) = 0 then 0
									else sqrt(((1.*(6.*n)*(n-1.)))/((n-2.)*(n+1.)*(n+3.)))
									end,
								d,n
							from (
									select 
										n,
										d,
										m2 = case n when 0 then 0 else sum(power((x-(sxf/n)),2)*f)/n end,
										m3 = case n when 0 then 0 else sum(power((x-(sxf/n)),3)*f)/n end
									from (
											select 
												x,
												f,
												sxf = 1.*sum(x*f) over(),
												n = sum(f) over(),
												d = count(x) over()
											from '+@TallyTable+'
											where [key] < @thiskey and ([key] > @lastkey or @lastkey is null)
										) base
									group by n,d
							) agg
					) sk
					where range_hi_key = @thiskey;
					if @@error != 0
						begin
							return
						end
					set @lastkey = @thiskey;
					fetch next from windowcur
						into @thiskey;			
				end
			close windowcur
			deallocate windowcur;';		
		--print @sql;
		exec sp_executesql @sql;
		
		if @@ERROR != 0
			begin
				select error_message(), @@ERROR, error_line()
				--print @sql;
				return
			end
		
		update tempdb.dbo.histo
			set table_name = @TableName,
				stat_name = @StatName
		where stat_name is null;

		fetch next from statcur into 
			@SchemaName, @TableName,@StatName,@StatColType,@StatColPrec,@StatColScale,@StatColMaxLen,@StatSteps;

		--drop table tempdb.dbo.histo;
	end
close statcur
deallocate statcur

--drop table tempdb.dbo.histo;


select an.*,confirm_query = 'select * from ' + @TallyTable + ' where ' +
case when cols.last_alt_key is not null then ' where x >= ' + cast(cols.last_alt_key as nvarchar(255)) + ' and ' else
	' x <=' + cast(cols.alt_key as nvarchar(255))
	end  + ' order by [key]'
from tempdb.dbo.histo an
join (
		select 
			stat_name,
			last_range_hi_key = lag(range_hi_key,1,null) over (partition by stat_name order by range_hi_key), 
			range_hi_key,
			last_alt_key = lag(alt_key,1,null) over (partition by stat_name order by range_hi_key), 
			alt_key
		from tempdb.dbo.histo
	) cols
on an.stat_name = cols.stat_name
	and an.range_hi_key = cols.range_hi_key
where an.actual_distinct_range_rows >= 100
order by abs(zg1) desc;
