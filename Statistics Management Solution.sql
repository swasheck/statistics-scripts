
create table [dbo].[StatisticsMonitor](
	[collection_time] [datetime] NOT NULL,
	[database_name] [sysname] NOT NULL,
	[schema_name] [sysname] NOT NULL,
	[table_name] [sysname] NOT NULL,
	[stat_name] [sysname] NOT NULL,
	[index_name] [sysname] NULL,
	[stat_column] [sysname] NULL,
	[stat_id] [int] NULL,
	[partition_number] [int] NULL,
	[is_filtered] [bit] NULL,
	[is_incremental] [bit] NULL,
	[is_autocreated] [bit] NULL,
	[last_updated] [datetime2](3) NULL,
	[rows] [bigint] NULL,
	[rows_sampled] [bigint] NULL,
	[unfiltered_rows] [bigint] NULL,
	[modification_counter] [bigint] NULL
);

go

create table [dbo].[StatsUpdateLog](
	[CollectionTime] [datetime2](3) NOT NULL,
	[DatabaseName] [sysname] NOT NULL,
	[SchemaName] [sysname] NOT NULL,
	[TableName] [sysname] NOT NULL,
	[StatisticsName] [sysname] NOT NULL,
	[StatisticsID] [int] NULL,
	[IndexName] [sysname] NULL,
	[IsIncremental] [bit] NOT NULL,
	[PartitionNumber] [int] NOT NULL DEFAULT ((1)),
	[StatisticsLeadingColumn] [sysname] NOT NULL,
	[StatisticsLastUpdated] [datetime2](3) NULL DEFAULT ('1970-01-01'),
	[StatisticsRows] [bigint] NULL DEFAULT ((0)),
	[StatisticsRowsSampled] [bigint] NULL DEFAULT ((0)),
	[StatisticsUnfilteredRows] [bigint] NULL DEFAULT ((0)),
	[StatisticsModificationCounter] [bigint] NULL DEFAULT ((0)),
	[StatisticsUpdateReason] [sysname] NULL,
	[ConfigModificationThreshold] [int] NULL DEFAULT ((0)),
	[ConfigSampleRateThreshold] [int] NULL DEFAULT ((0)),
	[ConfigHoursThreshold] [int] NULL DEFAULT ((0)),
	[UpdateStartTime] [datetime2](3) NULL,
	[UpdateEndTime] [datetime2](3) NULL,
	[UpdateSQL] [nvarchar](4000) NULL,
	[UpdateResult] [varchar](1000) NULL
);

go

create procedure [dbo].[dbasp_GetStatistics] (
	@ShowResults bit = 1,
	@DatabaseName sysname = null
											)
as
begin

truncate table DBALocationSupport.dbo.StatisticsMonitor;
set nocount on;
declare @dbname sysname;
declare @sql nvarchar(max);
declare @version int;


select @version = cast(left(cast(SERVERPROPERTY('productversion') as varchar(255)), 
		charindex('.',cast(SERVERPROPERTY('productversion') as varchar(255)))-1) as int);

declare dbcur cursor fast_forward read_only
	for 
		select name
		from sys.databases 
		where name != 'tempdb' and name not like 'ReportServer%'
			and database_id > 4
			and (name = @DatabaseName or @DatabaseName is null)
		;
open dbcur
fetch next from dbcur 
	into @dbname
while @@fetch_status = 0 
	begin 
		if @version > 11
		begin 
			set @sql = REPLACE(N'
						use !!!DBNAME!!!;
						select 
							getdate(),
							''!!!DBNAME!!!'',
							schema_name = sh.name,
							table_name = t.name,
							stat_name = s.name,
							index_name = i.name,
							index_col(quotename(sh.name)+''.''+quotename(t.name),s.stats_id,1),
							s.stats_id,
							isnull(sp.partition_number,1),
							s.has_filter,						
							s.is_incremental,
							s.auto_created,
							sp.last_updated,	
							sp.rows,
							sp.rows_sampled,						
							sp.unfiltered_rows,
							coalesce(sp.modification_counter, n1.modification_counter) modification_counter
						from sys.stats s 
						join sys.tables t 
							on s.object_id = t.object_id
						join sys.schemas sh
							on t.schema_id = sh.schema_id
						left join sys.indexes i 
							on s.object_id = i.object_id
								and s.name = i.name
						cross apply sys.dm_db_stats_properties_internal(s.object_id, s.stats_id) sp
						outer apply sys.dm_db_stats_properties_internal(s.object_id, s.stats_id) n1
						where 
							n1.node_id = 1
								and (
										(is_incremental = 0)
										or
										(is_incremental = 1 and sp.partition_number is not null)
									);','!!!DBNAME!!!',@dbname);
			end
		else
			begin
				set @sql = REPLACE(N'
						use !!!DBNAME!!!;
						select 
							getdate(),
							''!!!DBNAME!!!'',
							schema_name = sh.name,
							table_name = t.name,
							stat_name = s.name,
							index_name = i.name,
							index_col(quotename(sh.name)+''.''+quotename(t.name),s.stats_id,1),
							s.stats_id,
							partition_number = 1,
							s.has_filter,						
							is_incremental = 0,
							s.auto_created,
							sp.last_updated,	
							sp.rows,
							sp.rows_sampled,						
							sp.unfiltered_rows,
							sp.modification_counter
						from sys.stats s 
						join sys.tables t 
							on s.object_id = t.object_id
						join sys.schemas sh
							on t.schema_id = sh.schema_id
						left join sys.indexes i 
							on s.object_id = i.object_id
								and s.name = i.name
						cross apply sys.dm_db_stats_properties(s.object_id, s.stats_id) sp;','!!!DBNAME!!!',@dbname);
			end
		begin try
			insert into DBALocationSupport.dbo.StatisticsMonitor
			exec sp_executesql @sql;
		end try
		begin catch
			print @sql;
			select ERROR_MESSAGE();
		end catch
		fetch next from dbcur 
			into @dbname
	end 
close dbcur
deallocate dbcur

if @ShowResults = 1
	begin
		select * 
		from  DBALocationSupport.dbo.StatisticsMonitor;
	end


end;

go
-- exec [dbo].[dbasp_StatsMaintenance] @DatabaseName = 'DBArgus', @Execute = 0, @Verbose = 1, @NoIndexStats=0;
create procedure [dbo].[dbasp_StatsMaintenance] (
											@DatabaseName sysname = null,
											@ModThreshold int = 25,
											@HoursThreshold int = null,
											@MinSampleRate int = 5,																		
											@NoIndexStats bit = 1,
											@Execute bit = 1,											
											@Verbose bit = 0,
											@FullScanThreshold int = 500000
										)
as
	begin

	set nocount on;



	if @DatabaseName is null
	begin
		print 'DBASupport.dbo.dbasp_StatusUpdate Usage:
		@DatabaseName (sysname) = ALL (REQUIRED! No value will produce this dialog)
			Name of the database that contains the statistics to be updated.
			''ALL'' will run for all databases that are not tempdb 
		@ModThreshold (int) = 15 
			Minimum percentage of modifications to the leading column of the statistic
			that will trigger an update. By default, SQRT(100*Number of Rows In Statistic) will also trigger
			an update. @ModThreshold = 0 will update statistics with modifications >= SQRT(100*Number of Rows In Statistic).
		@HoursThreshold (int) = 0
			Minimum number of hours that have passed since the last statistics update. Default (NULL) will fall back to the number
			of hours since midnight of the most recent Monday.
		@MinSampleRate (int) = 5
			(NOT IMPLEMENTED!!! Reserved for future development)
			Minimum sample rate that we will tolerate. Statistics below this threshold will be resampled at the minimum sample rate.
		@NoIndexStats (bit) = 1
			Exclude Statistics that support an Index
		@FullScanThreshold (int) = 500000
			The number of rows at which we determine that we should no longer force the process to update at 100 percent sample rate
				(FULLSCAN)
		@Verbose bit = 1
			Verbose output (WARNING! Can be very verbose)
		@Execute bit = 0
			Execute the procedure?
			'
		return
	end

	exec DBASupport.dbo.dbasp_GetStatistics @ShowResults = 0, @DatabaseName = @DatabaseName;

	/*Check if Always On enabled and @DatabaseName is a secondary replica. If it is a secondary replica, process exits  */
	IF  SERVERPROPERTY ('IsHadrEnabled')=1
	BEGIN
		IF [dbo].[udf_IsPrimaryDatabase](DB_ID(@DatabaseName)) = 0 
			BEGIN
				PRINT 'Database '+ @DatabaseName + '  is part of an always on group and it is the secondary replica. Exiting'
				RETURN
			END
	END

	/*
		Set up the local variables
		@CollectionTime:
			Provides a uniform collection time to identify each update run
		@HoursThreshold:
			If not provided, default to midnight of last Monday
	*/
	declare @CollectionTime datetime2(3);
	set @CollectionTime = sysdatetime();	
	if @HoursThreshold is null
		begin
			select @HoursThreshold = datediff(hour,dateadd(week,-1,dateadd(week, datediff(week,0,sysdatetime()), 0)),sysdatetime())
		end

		/*
			Variables to be used in the cursors.
				@update_sql
					SQL that we will generate to update statistics objects
				@stats_group_id
					A way of identifying the statistics that we're update
				@start_time
					Establishes the start time of the individual stats update
				@end_time
					Establishes the end time of the individual stats update 
				@error_message
					If there's an error during the update, log this message
				@stats_sql
					SQL used to identify candidate statistics						
				@params
					Parameters that accompany @stats_sql to be fed to sp_executesql
		*/		
		declare @params nvarchar(1000);
		set @params = N'@CollectionTime datetime2(3),@ModThreshold int, @HoursThreshold int, @MinSampleRate int,@NoIndexStats bit';

		declare @schema_name sysname;
		declare @table_name sysname;
		declare @stat_name sysname;
		declare @stat_id int;
		declare @partition_number int;
		declare @sql nvarchar(4000);
		declare @error nvarchar(1000);		

		declare @start_time datetime2(3);
		declare @end_time datetime2(3);

		/*
			Identify AutoCreated statistics to be converted to named statistics objects
		*/
		print cast(sysdatetime() as nvarchar(255)) + ': begin renaming auto-created stats to something more intelligible'
		declare c cursor fast_forward read_only for 
			select 
				schema_name, 
				table_name,
				stat_name, 
				stat_id, 
				partition_number,
				'use ' + quotename(database_name) + ';' + 
					' drop statistics ' + quotename(schema_name) + '.' + quotename(table_name) + '.' + quotename(stat_name) + ';' + 
					' create statistics [sh_' + table_name + '_' + stat_column + '] on ' + quotename(schema_name) + '.' + quotename(table_name) + 
					' (' + quotename(stat_column) + ') with sample ' + cast(
																				case when rows <= @FullScanThreshold then 100 else isnull(nullif(cast((100.*rows_sampled)/unfiltered_rows as int),0),1) end
																			as nvarchar(3)) + 
					' percent' + 
					case (is_incremental) 
						when 1 then ', incremental = on;'
						else ';'
					end
			from DBALocationSupport.dbo.StatisticsMonitor
			where is_autocreated = 1;

		open c 
		fetch next from c into 
			@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;
		while @@fetch_status = 0
			begin
				set @error = 'success';
				set @start_time = sysdatetime();				
				begin try
					if @Execute = 1
					begin
						exec sp_executesql @sql;
					end
				end try
				begin catch
					print @sql;
					set @error = error_message();
				end catch

				set @end_time = sysdatetime();

				if @Verbose = 1
					begin
						print char(9) + cast(sysdatetime() as nvarchar(255)) + ': ' + @DatabaseName + '.' + @schema_name + '.' + 
							@table_name + '(' + @stat_name + ') ... ' + @error;
					end

				insert into DBALocationSupport.dbo.StatsUpdateLog
				( 
				  CollectionTime ,
				  DatabaseName ,
				  SchemaName ,
				  TableName ,
				  StatisticsName ,
				  StatisticsID ,
				  IndexName ,
				  IsIncremental ,
				  PartitionNumber ,
				  StatisticsLeadingColumn ,
				  StatisticsLastUpdated ,
				  StatisticsRows ,
				  StatisticsRowsSampled ,
				  StatisticsUnfilteredRows ,
				  StatisticsModificationCounter ,
				  StatisticsUpdateReason ,
				  ConfigModificationThreshold ,
				  ConfigSampleRateThreshold ,
				  ConfigHoursThreshold ,
				  UpdateStartTime ,
				  UpdateEndTime ,
				  UpdateSQL ,
				  UpdateResult
				)
				select 
					@CollectionTime,
					@DatabaseName,
					schema_name,
					table_name,
					stat_name,
					stat_id,
					index_name,
					is_incremental,
					partition_number,
					stat_column,
					last_updated,
					rows,
					rows_sampled,
					unfiltered_rows,
					modification_counter,
					'auto-created stat',
					@ModThreshold,
					@MinSampleRate,
					@HoursThreshold,
					@start_time,
					@end_time,
					@sql,
					@error					
				from DBALocationSupport.dbo.StatisticsMonitor 
					where database_name = @DatabaseName
						and schema_name = @schema_name
						and table_name = @table_name
						and stat_id = @stat_id
						and stat_name = @stat_name
						and partition_number = @partition_number;

				fetch next from c into 
					@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;


			end
		close c 
		deallocate c
		print cast(sysdatetime() as nvarchar(255)) + ': end renaming auto-created stats to something more intelligible';

		/*
			Update stats whose modification threshold is greater than configured value
		*/
		print cast(sysdatetime() as nvarchar(255)) + ': begin updating stats with modification pct > configured value (' + cast(@ModThreshold as nvarchar(3)) + ')';

		declare c cursor fast_forward read_only for
			select 			
				schema_name, 
				table_name,
				stat_name, 
				stat_id, 
				partition_number,
				'update statistics ' + quotename(sm.database_name) + '.' + quotename(sm.schema_name) + '.' + quotename(sm.table_name) + 
					'(' + quotename(sm.stat_name) + ') with ' + 
					case sm.is_incremental
						when 1 then 'resample on partitions(' + cast(sm.partition_number as nvarchar(3)) + ')'
						else 'sample ' + cast(
												case when rows <= @FullScanThreshold then 100 else isnull(nullif(cast((100.*rows_sampled)/unfiltered_rows as int),0),1) end
												as nvarchar(3)) + ' percent'
					end
			from DBALocationSupport.dbo.StatisticsMonitor sm
			where 
				database_name = @DatabaseName
				and (100.*isnull(modification_counter,1))/unfiltered_rows > @ModThreshold
				and (0 = @NoIndexStats or index_name is null)
				and not exists (
									select 1 
									from DBALocationSupport.dbo.StatsUpdateLog sl 
									where sl.DatabaseName = @DatabaseName
										and sl.TableName = sm.table_name
										and sl.StatisticsName = sm.stat_name
										and sl.CollectionTime = @CollectionTime
								)
		open c 
		fetch next from c into 
			@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;
		while @@fetch_status = 0
			begin
				set @error = 'success';
				set @start_time = sysdatetime();				
				begin try
					if @Execute = 1
					begin
						exec sp_executesql @sql;
					end					
				end try
				begin catch
					print @sql;
					set @error = error_message();
				end catch

				set @end_time = sysdatetime();

				if @Verbose = 1
					begin
						print char(9) + cast(sysdatetime() as nvarchar(255)) + ': ' + @DatabaseName + '.' + @schema_name + '.' + 
							@table_name + '(' + @stat_name + ') ... ' + @error;
					end

				insert into DBALocationSupport.dbo.StatsUpdateLog
				( 
				  CollectionTime ,
				  DatabaseName ,
				  SchemaName ,
				  TableName ,
				  StatisticsName ,
				  StatisticsID ,
				  IndexName ,
				  IsIncremental ,
				  PartitionNumber ,
				  StatisticsLeadingColumn ,
				  StatisticsLastUpdated ,
				  StatisticsRows ,
				  StatisticsRowsSampled ,
				  StatisticsUnfilteredRows ,
				  StatisticsModificationCounter ,
				  StatisticsUpdateReason ,
				  ConfigModificationThreshold ,
				  ConfigSampleRateThreshold ,
				  ConfigHoursThreshold ,
				  UpdateStartTime ,
				  UpdateEndTime ,
				  UpdateSQL ,
				  UpdateResult
				)
				select 
					@CollectionTime,
					@DatabaseName,
					schema_name,
					table_name,
					stat_name,
					stat_id,
					index_name,
					is_incremental,
					partition_number,
					stat_column,
					last_updated,
					rows,
					rows_sampled,
					unfiltered_rows,
					modification_counter,
					'high modification',
					@ModThreshold,
					@MinSampleRate,
					@HoursThreshold,
					@start_time,
					@end_time,
					@sql,
					@error					
				from DBALocationSupport.dbo.StatisticsMonitor 
					where database_name = @DatabaseName
						and schema_name = @schema_name
						and table_name = @table_name
						and stat_id = @stat_id
						and stat_name = @stat_name
						and partition_number = @partition_number;

				fetch next from c into 
					@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;


			end
		close c 
		deallocate c

		print cast(sysdatetime() as nvarchar(255)) + ': end updating stats with modification pct > configured value (' + cast(@ModThreshold as nvarchar(3)) + ')';

		/*
			Update stats whose time threshold is greater than configured value
		*/
		print cast(sysdatetime() as nvarchar(255)) + ': begin updating stats with timespan since last update > configured value (' + cast(@HoursThreshold as nvarchar(3)) + ')';

		declare c cursor fast_forward read_only for
			select 			
				schema_name, 
				table_name,
				stat_name, 
				stat_id, 
				partition_number,
				'update statistics ' + quotename(sm.database_name) + '.' + quotename(sm.schema_name) + '.' + quotename(sm.table_name) + 
					'(' + quotename(sm.stat_name) + ') with ' + 
					case sm.is_incremental
						when 1 then 'resample on partitions(' + cast(sm.partition_number as nvarchar(3)) + ')'
						else 'sample ' + cast(
												case when rows <= @FullScanThreshold then 100 else isnull(nullif(cast((100.*rows_sampled)/unfiltered_rows as int),0),1) end 
											as nvarchar(3)) + ' percent'
					end
			from DBALocationSupport.dbo.StatisticsMonitor sm
			where 
				database_name = @DatabaseName
				and (datediff(hour,last_updated,sysdatetime()) > @HoursThreshold and modification_counter > 0)
				and (0 = @NoIndexStats or index_name is null)
				and not exists (
									select 1 
									from DBALocationSupport.dbo.StatsUpdateLog sl 
									where sl.DatabaseName = @DatabaseName
										and sl.TableName = sm.table_name
										and sl.StatisticsName = sm.stat_name
										and sl.CollectionTime = @CollectionTime
								)
		open c 
		fetch next from c into 
			@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;
		while @@fetch_status = 0
			begin
				set @error = 'success';
				set @start_time = sysdatetime();				
				begin try
					if @Execute = 1
					begin
						exec sp_executesql @sql;
					end
				end try
				begin catch
					print @sql;
					set @error = error_message();
				end catch

				set @end_time = sysdatetime();

				if @Verbose = 1
					begin
						print char(9) + cast(sysdatetime() as nvarchar(255)) + ': ' + @DatabaseName + '.' + @schema_name + '.' + 
							@table_name + '(' + @stat_name + ') ... ' + @error;
					end

				insert into DBALocationSupport.dbo.StatsUpdateLog
				( 
				  CollectionTime ,
				  DatabaseName ,
				  SchemaName ,
				  TableName ,
				  StatisticsName ,
				  StatisticsID ,
				  IndexName ,
				  IsIncremental ,
				  PartitionNumber ,
				  StatisticsLeadingColumn ,
				  StatisticsLastUpdated ,
				  StatisticsRows ,
				  StatisticsRowsSampled ,
				  StatisticsUnfilteredRows ,
				  StatisticsModificationCounter ,
				  StatisticsUpdateReason ,
				  ConfigModificationThreshold ,
				  ConfigSampleRateThreshold ,
				  ConfigHoursThreshold ,
				  UpdateStartTime ,
				  UpdateEndTime ,
				  UpdateSQL ,
				  UpdateResult
				)
				select 
					@CollectionTime,
					@DatabaseName,
					schema_name,
					table_name,
					stat_name,
					stat_id,
					index_name,
					is_incremental,
					partition_number,
					stat_column,
					last_updated,
					rows,
					rows_sampled,
					unfiltered_rows,
					modification_counter,
					'hours threshold exceeded',
					@ModThreshold,
					@MinSampleRate,
					@HoursThreshold,
					@start_time,
					@end_time,
					@sql,
					@error					
				from DBALocationSupport.dbo.StatisticsMonitor 
					where database_name = @DatabaseName
						and schema_name = @schema_name
						and table_name = @table_name
						and stat_id = @stat_id
						and stat_name = @stat_name
						and partition_number = @partition_number;

				fetch next from c into 
					@schema_name,@table_name,@stat_name,@stat_id,@partition_number,@sql;


			end
		close c 
		deallocate c

		print cast(sysdatetime() as nvarchar(255)) + ': end updating stats with timespan since last update > configured value (' + cast(@HoursThreshold as nvarchar(3)) + ')';


		if @Execute = 0
			begin
				select * 
				from DBALocationSupport.dbo.StatsUpdateLog
				where CollectionTime = @CollectionTime;
				delete
				from DBALocationSupport.dbo.StatsUpdateLog
				where CollectionTime = @CollectionTime;
			end



	end
GO

