use greek; 


/*
	sp Parameters
*/

declare @SchemaName sysname = 'dbo',
		@TableName sysname = 'f_word',
		@ColumnName sysname,
		@SamplePercent int = -1;

/*
	Internal Variables
*/

declare @TableRowCount decimal(30,0); -- How many rows are in the table. Originally used to throw a warning, it now just reports on table cardinality
declare @sql nvarchar(max);
declare @TallyTable sysname; -- This is the name of the table that we're going to use to generate the distribution
declare @Parameters nvarchar(500);

create table tempdb.dbo.table_analysis ( -- this table will contained the statistics we're capturing on the cardinality of the columns
	schema_name sysname, 
	table_name sysname, 
	column_name sysname,
	b1 decimal(30,5),
	G1 decimal(30,5),
	ses decimal(30,5),
	distinct_values decimal(30,0),
	total_values decimal(30,0),
	distinct_ratio decimal(20,10),
	table_rows decimal(30,0),
	distinct_ratio_null decimal(20,10),
	Zg1 decimal(30,5),
	tally_table nvarchar(255)
										);


select @TableRowCount = sum(rows)
from sys.partitions p 
join sys.tables t 
	on p.object_id = t.object_id
join sys.schemas sh
	on sh.schema_id = t.schema_id
where t.name = @TableName
	and sh.name = @SchemaName



declare colcur cursor fast_forward read_only for -- get a list of the columns to inspect
	select 
		c.name	
	from sys.tables t 
	join sys.schemas sh
		on sh.schema_id = t.schema_id
	join sys.columns c 
		on t.object_id = c.object_id
	where 
		sh.name != 'sys'
		and t.is_ms_shipped = 0
		and (t.name = @Tablename or @TableName is null)
		and (c.name = @ColumnName or @ColumnName is null)
	order by t.name, c.column_id;

open colcur
fetch next from colcur 
	into @ColumnName

while @@fetch_status = 0 
	begin
		print @ColumnName;

		set @TallyTable = 'tempdb.dbo.[tt_'+@TableName+'_'+@ColumnName+']';


		/*
			adjusting the sample rate
				if the parameter is -1 then we're going to use the average of the sample rate of all stats on the table
				if the parameter is 0 then we're going to use the max sample rate of all stats on the table
				otherwise we'll use the sample rate that was requested by the user
		*/

		select
			@SamplePercent = 
			case 
				when @SamplePercent = -1 then isnull(nullif(cast(round(avg((100.*sp.rows_sampled)/sp.unfiltered_rows),0) as int),0),1)
				when @SamplePercent = 0 then isnull(nullif(cast(round(max((100.*sp.rows_sampled)/sp.unfiltered_rows),0) as int),0),1)				
				else @SamplePercent
			end
		from sys.tables t 
		join sys.stats s 
			on t.object_id = s.object_id
		join sys.columns c 
			on t.object_id = c.object_id	
		join sys.schemas sh
			on sh.schema_id = t.schema_id
		cross apply sys.dm_db_stats_properties(s.object_id,s.stats_id) sp
		where s.has_filter = 0
			and t.name = @TableName
			and sh.name = @SchemaName
			and c.name = @ColumnName
			and index_col(sh.name + '.' + t.name,s.stats_id,1) = @ColumnName

		
		-- clean up old tables (if they exist)
		if exists (select 1 from tempdb.sys.tables where name = 'tt_'+@TableName+'_'+@ColumnName)
			begin
				set @sql = 'drop table ' + @TallyTable;
				exec sp_executesql @sql;
			end

		/*
			generate the base distibution histogram ... value, freq(value) 
				ranking values over the distribution allows us to index later. additionally 
					histogram buckets are traditionally integers (even within a range)				 
		*/

		set @sql = 
				'select 
					[key] = '+quotename(@ColumnName)+', 
					x = dense_rank() over (order by '+quotename(@ColumnName)+'), 
					f = count_big(1)			
				into  '+@TallyTable+'
				from '+quotename(@SchemaName)+'.'+quotename(@TableName)+'
				tablesample	system ('+cast(@SamplePercent as nvarchar(3))+' percent)
				where '+quotename(@ColumnName)+' is not null
				group by '+quotename(@ColumnName)

		print @sql;

		exec sp_executesql @sql;

		-- indexing for fun and profit
		set @sql = 'create clustered index cix_temp on '+@TallyTable+'(x,f)';
		exec sp_executesql @sql;		

		/*
			calculate various skewness stats. most are self-explanatory
				b1 -> population skewness calculation
				G1 -> sample skewness calculation (corrects for sample bias)
				ses -> standard error of skew. this is a standard deviation calculation on the G1
				Zg1 -> test statistic which provides more meaning to the skewness calculation
		*/

		set @sql = 
		'insert tempdb.dbo.table_analysis
		        ( schema_name ,
		          table_name ,
		          column_name ,
		          b1 ,
		          G1 ,
		          ses ,
		          distinct_values ,
		          total_values ,
		          distinct_ratio ,
		          table_rows ,
		          distinct_ratio_null ,
		          Zg1,
				  tally_table
		        )
		select 
			''' + @SchemaName + ''',
			''' + @TableName + ''',
			''' + @ColumnName + ''',
			sk.b1,
			sk.G1,
			sk.ses,
			d,
			n,
			(100. * d)/n,
			'+cast(@TableRowCount as nvarchar(255))+',
			(100.*d)/'+cast(@TableRowCount as nvarchar(255))+',
			zG1 = case sk.ses when 0 then sk.G1 else sk.G1/sk.ses end,
			'''+@TallyTable+'''		
		from (
				select 
					b1 =
						case when (power(m2,1.5)) = 0 then 0 
						else m3 / (power(m2,1.5))
						end,
					G1 = (sqrt(1.*(n*(n-1)))/(n-2)) * 
						case when (power(m2,1.5)) = 0 then 0 
						else m3 / (power(m2,1.5))
						end,
					ses = case when ((n-2.)*(n+1.)*(n+3.)) = 0 then 0
						else sqrt(((1.*(6.*n)*(n-1.)))/((n-2.)*(n+1.)*(n+3.)))
						end,
					d,n
				from (
						select 
							n,
							d,
							m2 = sum(power((x-(sxf/n)),2)*f)/n,
							m3 = sum(power((x-(sxf/n)),3)*f)/n
						from (
								select 
									x,
									f,
									sxf = 1.*sum(x*f) over(),
									n = sum(f) over(),
									d = count(x) over()
								from '+@TallyTable+'
							) base
						group by n,d
				) agg
		) sk';
		print @sql;
		exec sp_executesql @sql;

		fetch next from colcur 
			into @ColumnName
	end
close colcur
deallocate colcur

select *, 
	test_sql = 'select * from ' + tally_table + ' order by [x] asc;',
	cleanup_sql = 'drop table ' + tally_table + ';'
from tempdb.dbo.table_analysis
where abs(Zg1) >= 2
	and distinct_values >= 100
order by abs(Zg1) desc

--drop table tempdb.dbo.table_analysis