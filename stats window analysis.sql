use idw;
drop table tempdb.dbo.histo;

select 
	[key] = date_id, 
	x = dense_rank() over (order by date_id), 
	f = count_big(1) 
into #agg
from dbo.f_holdinghist 			
group by date_id
order by date_id asc;
create clustered index cix_aggtmp on #agg (x,f);
create table tempdb.dbo.histo (
						table_name sysname null, 
						stat_name sysname null,
						range_hi_key int, 
						range_rows decimal(30,3), 
						eq_rows bigint, 
						distinct_range_rows bigint,
						avg_range_rows decimal(30,3),
						actual_range_rows decimal(30,3),
						actual_eq_rows decimal(30,3),
						actual_distinct_range_rows bigint, 
						actual_avg_range_rows decimal(30,3),						
						b1_skewness decimal(30,3),
						G1_skewness decimal(30,3),
						ses decimal(30,5),
						Zg1 decimal(30,3)						
					);
declare @sql nvarchar(max) = 'dbcc show_statistics(''[dbo].[f_holdinghist]'',''IDX_Cluster_holdings'') with histogram,no_infomsgs;';
insert into tempdb.dbo.histo (
						range_hi_key , 
						range_rows , 
						eq_rows , 
						distinct_range_rows ,
						avg_range_rows )
		execute sp_executesql @sql;		
update h 
set h.actual_eq_rows = a.f	
from tempdb.dbo.histo h 
join #agg a
	on a.[key] = h.range_hi_key;
declare @lastkey int, @thiskey int;
declare windowcur cursor fast_forward read_only
	for 
	select	range_hi_key 
	from tempdb.dbo.histo
	order by range_hi_key asc;
open windowcur;
fetch next from windowcur
	into @thiskey;
while @@fetch_status = 0
	begin
			update tempdb.dbo.histo 
			set 
				actual_range_rows = isnull(n,0),
				actual_avg_range_rows = case when n is null then 0 else (1.*d)/n end,
				actual_distinct_range_rows = isnull(d,0),
				b1_skewness = sk.b1,
				G1_skewness = sk.G1,
				ses = sk.ses,
				Zg1 = case sk.ses when 0 then sk.G1 else sk.G1/sk.ses end
			from (
					select 
						b1 =
							case when (power(m2,1.5)) = 0 then 0 
							else m3 / (power(m2,1.5))
							end,
						G1 = 
							case n when 2 then 0 
							else (sqrt(1.*(n*(n-1)))/(n-2)) * 
								case when (power(m2,1.5)) = 0 then 0 
								else m3 / (power(m2,1.5))
								end
							end,
						ses = case when ((n-2.)*(n+1.)*(n+3.)) = 0 then 0
							else sqrt(((1.*(6.*n)*(n-1.)))/((n-2.)*(n+1.)*(n+3.)))
							end,
						d,n
					from (
							select 
								n,
								d,
								m2 = case n when 0 then 0 else sum(power((x-(sxf/n)),2)*f)/n end,
								m3 = case n when 0 then 0 else sum(power((x-(sxf/n)),3)*f)/n end
							from (
									select 
										x,
										f,
										sxf = 1.*sum(x*f) over(),
										n = sum(f) over(),
										d = count(x) over()
									from #agg
									where [key] < @thiskey and ([key] > @lastkey or @lastkey is null)
								) base
							group by n,d
					) agg
			) sk
			where range_hi_key = @thiskey;
			if @@error != 0
				begin
					return
				end
			set @lastkey = @thiskey;
			fetch next from windowcur
				into @thiskey;			
		end
	close windowcur
	deallocate windowcur;
--select *, 
--	'	select [key] = date_id, 
--			x = dense_rank() over (order by date_id), 
--			f = count_big(1) from dbo.f_holdinghist where date_id > ' + cast(prevcol.last_range_hi_key as nvarchar(255)) + '
--		and date_id < ' + cast(prevcol.range_hi_key as nvarchar(255)) + '
--		group by date_id
--		order by date_id asc;'
--from tempdb.dbo.histo h
--cross apply (
--				select 
--					range_hi_key,
--					last_range_hi_key = lag(range_hi_key,1,null) over (order by range_hi_key asc) 
--				from tempdb.dbo.histo h2	
--			) prevcol
--where actual_distinct_range_rows > 100
--	and h.range_hi_key = prevcol.range_hi_key
--order by abs(Zg1) desc